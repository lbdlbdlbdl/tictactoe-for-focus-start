package controller;

import java.util.function.Function;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.Node;
import model.GameModel;
import model.Point;

/**
 * Created by Al on 09.10.2016.
 */
public class ViewController implements EventHandler<ActionEvent> {

    private GameModel gameModel;
    private AnchorPane anchorPane;

    @FXML
    private Button restartButton;

    @FXML
    public void handleRestart(ActionEvent event) {
        restartButton = (Button) event.getSource();
        initializeEmptyButtons();
        gameModel = new GameModel();
        disableButtons(false);
    }

    @Override
    public void handle(ActionEvent event) {
        Button source = (Button) event.getSource();
        String buttonName = source.getId();
        source.setText("X");

        if (gameModel.getNextTurn()) {
            Point computerPoint = gameModel.move(toInt(buttonName.charAt(6)), toInt(buttonName.charAt(7)));
            setButtonText(computerPoint);
        }
        if (gameModel.getGameState() != null) {
            disableButtons(true);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Winner");
            alert.setHeaderText(null);
            switch (gameModel.getGameState()) {
                case X_WIN:
                    alert.setContentText("You win");
                    break;
                case O_WIN:
                    alert.setContentText("Computer win");
                    break;
                case DRAW:
                    alert.setContentText("Draw");
                    break;
            }
            alert.showAndWait();
        }
    }

    private void disableButtons(boolean isDisable) {
        ObservableList<Node> paneChildren = anchorPane.getChildren();
        for (Node childNode : paneChildren) {
            if (childNode instanceof Button && !childNode.getId().equals("buttonRestart")) {
                final Button button = (Button) childNode;
                button.setDisable(isDisable);
            }
        }
    }

    private void initializeEmptyButtons() {
        ObservableList<Node> paneChildren = anchorPane.getChildren();
        for (Node childNode : paneChildren) {
            if ((childNode instanceof Button) && (!childNode.equals(restartButton))) {
                Button button = (Button) childNode;
                button.setText(" ");
            }
        }
    }


    private void setButtonText(Point computerPoint) {
        Character computerPointX = toChar(computerPoint.getX());
        Character computerPointY = toChar(computerPoint.getY());

        ObservableList<Node> paneChildren = anchorPane.getChildren();
        for (Node childNode : paneChildren) {
            if (childNode instanceof Button && !childNode.getId().equals("buttonRestart")) {
                final Button button = (Button) childNode;
                if (button.getId().charAt(6) == computerPointX && button.getId().charAt(7) == computerPointY) {
                    button.setText("O");
                }
            }
        }
    }

    private Character toChar(int i) {
        return Integer.toString(i).charAt(0);
    }

    private int toInt(char character) {
        return Character.getNumericValue(character);
    }

    public void setGameModel(GameModel gameModel) {
        this.gameModel = gameModel;
    }

    public void setAnchorPane(AnchorPane anchorPane) {
        this.anchorPane = anchorPane;
    }
}

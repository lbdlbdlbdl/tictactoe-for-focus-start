package model;

/**
 * Created by Al on 09.10.2016.
 */
public enum GameState {
    X_WIN,
    O_WIN,
    DRAW;
}

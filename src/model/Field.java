package model;

/**
 * Created by Al on 09.10.2016.
 */
public class Field {

    private static final int DEFAULT_BOARD_SIZE = 3;

    private final Figure[][] figures = new Figure[DEFAULT_BOARD_SIZE][DEFAULT_BOARD_SIZE];

    void setFigure(final int x, final int y, final Figure figure) {
        figures[x][y] = figure;
    }

    Figure getFigure(final int x, final int y) {
        return figures[x][y];
    }

    Figure[][] getFigures() {
        return figures;
    }
}

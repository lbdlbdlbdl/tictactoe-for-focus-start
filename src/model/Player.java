package model;

/**
 * Created by Al on 09.10.2016.
 */
public class Player {

    private final Figure figure;

    Player(final Figure figure) {
        this.figure = figure;
    }

    Figure getFigure() {
        return figure;
    }


}

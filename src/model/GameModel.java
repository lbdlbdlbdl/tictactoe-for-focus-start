package model;

import java.util.Date;
import java.util.Random;


/**
 * Created by Al on 09.10.2016.
 */

public class GameModel {

    private final Field field;
    private final Player[] players;
    private final int fieldLength;
    private int moveCount;

    public GameModel() {
        this.field = new Field();
        this.fieldLength = field.getFigures().length;
        players = new Player[2];
        players[0] = new Player(Figure.X);
        players[1] = new Player(Figure.O);
        moveCount = 0;
    }

    public Point move(final int x, final int y) {
        if (field.getFigure(x, y) == null) {
            field.setFigure(x, y, Figure.X);
        }
        Point computerMovePoint = computerMove();
        moveCount += 2;
        return computerMovePoint;
    }

    public boolean getNextTurn() {
        if (getGameState() != null) {
            return false;
        }
        for (Figure[] figureArray : field.getFigures()) {
            for (Figure figureValue : figureArray) {
                if (figureValue == null) {
                    return true;
                }
            }
        }
        return false;
    }

    public GameState getGameState() {
        int mainDiag, supDiag, hor, ver;
        for (Player playerForCheck : players) {
            Figure figureToCheck = playerForCheck.getFigure();
            for (int i = 0; i < fieldLength; i++) {
                hor = 0;
                ver = 0;
                for (int j = 0; j < fieldLength; j++) {
                    if (field.getFigure(i, j) == figureToCheck) { // horizontal
                        hor++;
                    }
                    if (field.getFigure(j, i) == figureToCheck) { //vertical
                        ver++;
                    }
                }
                if (hor == 3 || ver == 3) {
                    return getWinner(playerForCheck);
                }
            }
            mainDiag = 0;
            supDiag = 0;
            for (int i = 0; i < fieldLength; i++) {
                if (field.getFigure(i, i) == figureToCheck) { // main diagonal
                    mainDiag++;
                }
                if (field.getFigure(i, 2 - i) == figureToCheck) { // sup diagonal
                    supDiag++;
                }
            }
            if (mainDiag == 3 || supDiag == 3) {
                return getWinner(playerForCheck);
            }
        }
        if (moveCount == 8) {
            return GameState.DRAW;
        }

        return null;
    }

    private GameState getWinner(Player player) {
        if (player.getFigure() == Figure.O) {
            return GameState.O_WIN;
        } else {
            return GameState.X_WIN;
        }
    }

    private Point computerMove() {
        int x, y;
        Random random = new Random(new Date().getTime());
        do {
            x = random.nextInt(3);
            y = random.nextInt(3);
        } while (field.getFigure(x, y) != null);
        Point point = new Point(x, y);
        field.setFigure(x, y, Figure.O);
        return point;
    }

}

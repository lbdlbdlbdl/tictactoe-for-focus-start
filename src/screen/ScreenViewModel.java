package screen;

/**
 * Created by Al on 09.10.2016.
 */

import java.io.IOException;

import controller.ViewController;
import model.GameModel;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


public class ScreenViewModel extends Application {

    private AnchorPane anchorPane;

    @Override
    public void start(Stage stage) {
        FXMLLoader loader = new FXMLLoader();
        initLayout(stage, loader);
        GameModel gameModel = new GameModel();
        ViewController viewController = loader.getController();
        viewController.setGameModel(gameModel);
        viewController.setAnchorPane(anchorPane);
    }

    private void initLayout(Stage stage, FXMLLoader loader) {
        try {
            loader.setLocation(ScreenViewModel.class.getResource("layout.fxml"));
            AnchorPane anchorPaneRoot = loader.load();
            anchorPane = anchorPaneRoot;
            Scene scene = new Scene(anchorPaneRoot);
            stage.setScene(scene);
            stage.setResizable(false);
            stage.setTitle("TicTacToe");
            stage.show();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ScreenViewModel.launch();
    }
}
